# Ruche Fille

Les ruches dites "filles" sont composées d'un Arduino de la famille ATMega, d'un capteur de température DS18B20, d'un module de communication NRF24L01+PA et qu'un converstisseur Analogique/Numérique ADS1015 relié à un capteur de flexion afin de mesurer la Masse. 

## Rôle Ruche Fille

Elle mesure la température intérieure, la masse et transmet ces données à la ruche Mère.

## Matériel

- [ ] - Arduino de type ATmega [Liste Cartes Arduino](https://en.wikipedia.org/wiki/List_of_Arduino_boards_and_compatible_systems)
- [ ] - NRF24L01+PA [NRF24L01+PA](http://www.nordicsemi.com/eng/Products/2.4GHz-RF/nRF24L01)
- [ ] - DS18B20 [DS18B20](https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf)
- [ ] - ADS1015 sur module [ADS1015](https://www.adafruit.com/product/1083)
- [ ] - Capteur de flexion [Principe Mesure et Wheatstone](http://pesage.biz/le-pesage-et-ses-techniques/capteurs-de-pesage-a-jauges-de-contrainte-load-cell/)

> Faites bien attention à la qualité de la connexion du capteur de flexion, nous cherchons à mesurer des millivolts au bout d'un cable assez long, utilisez bien le blindage et gardez en mémoire que la moindre perturbation électro-magnétique influera sur votre mesure

## Cablage

Arduino         | NRF24L01+     | DS18B20       | ADS1015       | Capteur Flexion
-------------   | ------------- | ------------  | ------------  | ------------
3.3V            | VCC           |               |               |
GND             | GND           |               |               |
D7              | CE            |               |               |
D13             | SCK           |               |               |
D12             | MISO          |               |               |
D11             | MOSI          |               |               |
D8              | CSN           |               |               |
---             |               |               |               |
5V              |               | VCC           |               |
GND             |               | GND           |               |
D2              |               | DATA          |               |
---             |               |               |               |
Aref            |               |               | VCC           | VCC
GND             |               |               | GND           | GND
SDA             |               |               | SDA           |
SCL             |               |               | SCL           |
---             |               |               | IN A          | E+ Balance
---             |               |               | IN B          | E- Balance



> Faites bien attention à Polariser (alimenter) le capteur de flexion avec les mêmes potentiels que le convertisseur Analogique/Numérique