# BOM (Bill of Materials)

## Ruche Fille

- [ ] Arduino Pro Micro (3.3V)

- [ ] NRF24L01+

- [ ] Mesures
    - [ ] Température (DS18B20)
    - [ ] Masse amplification et conversion (ADS1015)
    - [ ] Masse jauge de contrainte (YZC 644)
