/*
  Ruche Fille Pojet BTS SNIR GRETA Arpajon 2018

  [*] DS18B20
  [*] NRF24
  [*] RF24 Mesh Networking
  [/] ADS1115


*/

//NRF24L01+
#include <RF24.h>
#include <RF24Network.h>
#include <RF24Mesh.h>
#include <SPI.h>
#include <EEPROM.h>
// ID de la ruche
uint8_t ID = 111; //1 à 255 (254 is for ask a new ID)
int IDaddr = 344; // EEPROM ID address
int apLED = 9;
int incomingByte = 0;   // for incoming serial data
//ID du Rucher 1 à 126 correspond au Channel avance par pas de 1MHz 1=2400MHz 126=2525MHz (12,13,14, interdit aux USA)
const int IDrucher = 1;  // 90 = 2489MHz

RF24 radio(7, 8);                    // Configuration des Pins CE et CSN
RF24Network network(radio);          // Network sur cette radio
RF24Mesh mesh(radio, network);       // Mesh sur ce Network

const unsigned long interval = 1000; // Interval entre les mesures en ms
const unsigned long intervalAP = 600000; // Interval entre les mesures en ms

unsigned long last_sent;             // Compteur dernier envoi


struct payload_t {                   // Structure des données à trasnmettre
  const uint8_t IDfille;
  uint8_t masse;
  int8_t temp;
};

// DS18B20
#include <OneWire.h>
#include <DallasTemperature.h>
#define ONE_WIRE_BUS 2
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
int temp = 0;

//ADS 1115
#include <Wire.h>
#include <Adafruit_ADS1015.h>
Adafruit_ADS1115 ads;
uint16_t masse = 0;
int16_t results;
  /* Correspond au pas 1bit = 1mV  */
float   multiplier = 0.125F;    /* ADS1015 @ +/- 6.144V gain (12-bit results) */


void startAP(void) {
  Serial.println("Start AP");
  mesh.releaseAddress();
  mesh.renewAddress();
  ID = 254;
  mesh.setNodeID(ID);
  Serial.println("Set ID 254");
  mesh.update();
  Serial.println("Mesh Update"); 
  unsigned long now = millis();
  payload_t payload = { ID, 0, 0 };
  RF24NetworkHeader header;
  bool ok = mesh.write(&payload, 'M', sizeof(payload));
  if (ok) {
    last_sent = now;  
    Serial.print("Demande AP OK ID: ");
    Serial.println(mesh.getNodeID());
    Serial.print("From Node ID: ");
    Serial.println(header.from_node);
  } else {   
    Serial.println("Demande AP Failed ");
    return;
  }  
  Serial.println("Start Waiting Response");
  while (millis() - last_sent < intervalAP) {
    mesh.update();
    if (network.available()) {   
      Serial.println("Get Response");
      RF24NetworkHeader header;
      network.peek(header);
      Serial.print("from Node: ");
      Serial.println(header.from_node);
      uint32_t dat = 0;
      Serial.print("Header Type: ");
      Serial.println(header.type);
      network.read(header, &dat, sizeof(dat));
      Serial.print("Data: ");
      Serial.println(dat);
      ID = dat;
      
      EEPROM.write(IDaddr, dat);
      Serial.print("EEPROM WRITE ADDR: ");
      Serial.println(IDaddr);
      Serial.print("EEPROM WRITE DATA: ");
      Serial.println(dat);
     
      mesh.setNodeID(ID);
      return;
    }
  }
}

void setup(void)
{

  pinMode(apLED, OUTPUT);
  digitalWrite(apLED, LOW);
  // Setup Serial
  Serial.begin(115200);
  Serial.println("Ruche Fille Serial Debug");
  ID = EEPROM.read(IDaddr);
  if (ID > 250) {
    ID = 254;
  }
  Serial.print("ID read from EEPROM: ");
  Serial.println(ID);
  // Setup RF24
  mesh.setNodeID(ID);
  Serial.print("ID Set At Boot: ");
  Serial.println(mesh.getNodeID());
  // Connect to the mesh
  Serial.println(F("Connection au réseau maillé..."));
  mesh.begin(IDrucher, RF24_1MBPS, MESH_RENEWAL_TIMEOUT);
  //mesh.releaseAddress();
  mesh.renewAddress();
  // Setup OneWire
  sensors.begin();

  ///Setup ADS115
  ads.setGain(GAIN_SIXTEEN);        // 2x gain   +/- 2.048V  1 bit = 1mV 
  ads.begin();
}

void loop() {


  mesh.update();                          // Verifier la connection au réseau maillé
  
  if (Serial.available() > 0) {
    incomingByte = Serial.read();
    Serial.println(incomingByte);
    Serial.println(incomingByte, DEC);
    if (incomingByte == 48) {
      Serial.println("Reset By Serial");
      startAP();
    }
  }
  unsigned long now = millis();              // Si pas d'envoi depuis l'intervalle défini on envoie
  if ( now - last_sent >= interval  )
  {
    if (ID == 254) {
      startAP();
    }
    // Démarrer les mesures
    sensors.requestTemperatures();
    temp = sensors.getTempCByIndex(0);
    results = ads.readADC_Differential_0_1();  
   masse = (results * 819 * multiplier)-3276;

    Serial.print("Envoi... ID: ");
    Serial.print(ID);
    Serial.print(" Temp: ");
    Serial.print(temp);
    Serial.print(" Masse: ");
    Serial.println(masse);

    // Envoie des données au rucher maitre
    payload_t payload = { ID, masse, temp };
    RF24NetworkHeader header;
    bool ok = mesh.write(&payload, 'M', sizeof(payload));

    if (ok) {
      last_sent = now;
      Serial.print("Envoi OK From ID: ");
      Serial.println(mesh.getNodeID());
    }
    else {
      Serial.print("Echec Envoi From ID: ");
      Serial.println(mesh.getNodeID());

      // Si l'envoi échoue vérifier la connection au réseau
      if ( ! mesh.checkConnection() ) {
        // Rafraichir adresse réseau
        Serial.println("Renouveller Adresse");
        mesh.releaseAddress();
        mesh.renewAddress();
      } else {
        Serial.println("Erreur Renouvellement Adresse");
      }
    }
  }
}

