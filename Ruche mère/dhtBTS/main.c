#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int pi_2_dht_read(int, int, float*, float*);

int main()
{
	float hum, temp = 0.0f;

	while (1)
	{
		if (pi_2_dht_read(22, 4, &hum, &temp) == 0)
		{
			printf("humidite %0.2f temperature %0.2f\n", hum, temp);
		}
		usleep(20000);
	}
	return 0;
}
