# Ruche Mère

La ruche dite "mère" est composée d'un RaspberryPi, d'un capteur de température/humidité DHT22, d'un module de communication NRF24L01+PA, d'un GPS et d'une clé GSM/3G.

## Rôle Ruche Mère

Elle centralise les mesures des ruches filles, elle joue aussi le rôle de noeud principal de réseau, c'est elle qui gère la topologie du réseau.

La ruche mère doit aussi mesurer la température et l'humidité extérieure ainsi que la position GPS du rûcher.
La ruche "mère" disposant d'un module GSM c'est aussi elle qui gère les alertes par SMS et l'envoie des données au serveur.

## Matériel

- [ ] - Raspberry Pi 2 ou 3 [Raspberry](https://www.raspberrypi.org)
- [ ] - NRF24L01+PA [NRF24L01+PA](http://www.nordicsemi.com/eng/Products/2.4GHz-RF/nRF24L01)
- [ ] - DHT22 [DHT22](https://cdn-shop.adafruit.com/datasheets/Digital+humidity+and+temperature+sensor+AM2302.pdf)
- [ ] - Clé USB HSDPA [Basée sur Qualcomm MSM6280](https://en.wikichip.org/wiki/qualcomm/msm6xxx/msm6280)
- [ ] - GPS USB [Haicom HI-204e](https://www.nauticom.fr/files/doc/gps/HI204EManuelFR.pdf)

> Faites bien attention à prévoir une alimentation adéquate pour le RaspberryPi et prévoir un Hub Alimenté sur lequel brancher les clefs GPS et GSM ces dernières pouvant consommer beaucoup de courant.

## Cablage

RaspberryPi     | NRF24L01+     | DHT22
-------------   | ------------- | ------------
3.3V            | VCC           | 
GND             | GND           |
15 (GPIO_22)    | CE            |
23 (SPI0_CLK)   | SCK           |
21 (SPI0_MISO)  | MISO          |
19 (SPI0_MOSI)  | MOSI          |
24 (SPI_CE0_N)  | CSN           |
---             |               |
5V              |               | VCC
GND             |               | GND
7 (GPIO_4)      |               | DATA

> Si vous utilisez un modèle de DHT22 "nu" n'oubliez pas la résistance (2 kOhms à 10 kOhms) de "pull-up" entre VCC et DATA

## Logiciels

- [ ] - RF24 Class

```bash
git clone https://github.com/TMRh20/RF24.git
cd RF24
sudo make install -B
sudo apt-get install python-dev libboost-python-dev
sudo apt-get install python-setuptools
cd pyRF24
sudo python ./setup.py build
sudo python ./setup.py install
cd ..

git clone https://github.com/tmrh20/RF24Network.git RF24Network
cd RF24Network
sudo make install

cd ..
git clone https://github.com/nRF24/RF24Mesh.git RF24Mesh
cd RF24Mesh
sudo make install
```

- [ ] - Connecteur MySQL C++

```bash
sudo apt-get install libmysqlclient-dev libmysqlcppconn-dev
```

- [ ] - DHT22

```bash
cd ~
sudo apt-get update
sudo apt-get install build-essential python-dev
git clone  https://github.com/adafruit/Adafruit_Python_DHT.git
cd Adafruit_Python_DHT/
sudo python setup.py install
```

## Compilation