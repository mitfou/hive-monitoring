
//Include pour le NRF24
#include "RF24Mesh/RF24Mesh.h"  
#include <RF24/RF24.h>
#include <RF24Network/RF24Network.h>

#include <stdlib.h>
#include <iostream>

//Include pour SQL
#include "mysql_connection.h"
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

using namespace std;

// Prototypes des fonctions
void putInDB(payload_t payload);
void putNewHive(int newIDtoPut);
int getLastID();
void startAP(int fromNode);

// Structure d'un paquet
struct payload_t {
  uint8_t IDfille;
  uint8_t masse;
  int8_t temp;
};


//Instanciation des objets
const int IDrucher = 1;
int newID = 254;
int IDfille = 0;
RF24 radio(RPI_V2_GPIO_P1_15, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_8MHZ);  
RF24Network network(radio);
RF24Mesh mesh(radio,network);
payload_t payload; 

//main
int main(int argc, char** argv) {
  
  // Initialiser le réseau maillé
  mesh.setNodeID(0);
  // Connect to the mesh
  printf("start\n");
  mesh.begin(IDrucher,RF24_1MBPS,MESH_RENEWAL_TIMEOUT);
  radio.printDetails();
  printf("Apairrage Exemple\nNode ID: %d\n",mesh.getNodeID());
while(1)
{
  
  // Call network.update as usual to keep the network updated
  mesh.update();

  // In addition, keep the 'DHCP service' running on the master node so addresses will
  // be assigned to the sensor nodes
  mesh.DHCP();
  
  
  // Check for incoming data from the sensors
  while(network.available()){
    printf("rcv\n");
    RF24NetworkHeader header;
    network.peek(header);
    switch(header.type){
      // Display the incoming millis() values from the sensor nodes
      case 'M': network.read(header,&payload,sizeof(payload)); 
                IDfille = payload.IDfille;
                 printf("Rcv Type: %c from Node: %d\n",header.type,header.from_node);
                printf("Received from ID: %d Masse: %d  Temp: %d \n",payload.IDfille,payload.masse,payload.temp);
                 if(IDfille==254){
                     startAP(header.from_node);
                 }
                 else {
                      putInDB(payload);
                 }
                 
                 break;
      default:  network.read(header,0,0); 
                printf("Rcv bad type %d from 0%o\n",header.type,header.from_node); 
                break;
    }
  }
delay(50);
  }
return 0;
}

void putInDB(payload_t payload){

if(IDfille>=254){
    cout << "254 Max Errord" << endl;
    return;
  }
  mesh.update();
  mesh.DHCP();
 try {
  sql::Driver *driver;
  sql::Connection *con;
  sql::PreparedStatement *pstmt;
  // Create a connection 
  driver = get_driver_instance();
                printf("Put In DB from ID: %d Masse: %d  Temp: %d \n",payload.IDfille,payload.masse,payload.temp);
  con = driver->connect("tcp://155.94.213.167:3306", "asianhornet", "aRpa18");
  // Connect to the MySQL test database 
  con->setSchema("ruchedb");
  pstmt = con->prepareStatement("INSERT INTO ruchedb.satellite_info (temperature, weight, timestamp, satellite_id) VALUES (?, ?, CURRENT_TIMESTAMP, ?)");
  pstmt->setDouble(1, payload.temp);
  pstmt->executeUpdate();
  pstmt->setDouble(2, payload.masse);
  pstmt->executeUpdate();
  pstmt->setInt(3, payload.IDfille);
  pstmt->executeUpdate();
  delete pstmt;
  delete con; 
} 
catch (sql::SQLException &e) {
  cout << "# ERR: SQLException in " << __FILE__;
 // cout << "(" << __FUNCTION__ << ") on line " »
   //  << __LINE__ << endl;
  cout << "# ERR: " << e.what();
  cout << " (MySQL error code: " << e.getErrorCode();
  cout << ", SQLState: " << e.getSQLState() << " )" << endl;
}
}


void putNewHive(int newIDtoPut){
  if(newIDtoPut==254){
    cout << "254 Error Max Hive Reached" << endl;
    return;
  }
  mesh.update();
  mesh.DHCP();
 try {
  sql::Driver *driver;
  sql::Connection *con;
  sql::Statement *stmt;
  sql::PreparedStatement *pstmt;
  sql::ResultSet *res;

  // Create a connection 
  driver = get_driver_instance();
  cout << "Put New Hive: " << newIDtoPut << endl;
  con = driver->connect("tcp://155.94.213.167:3306", "asianhornet", "aRpa18");
  // Connect to the MySQL test database 
  con->setSchema("ruchedb");
  pstmt = con->prepareStatement("INSERT INTO ruchedb.hive_satellite (id, timestamp, hive_base_id) VALUES (?, CURRENT_TIMESTAMP, ?)");
  pstmt->setInt(1, newIDtoPut);
  pstmt->executeUpdate();
  pstmt->setInt(2, IDrucher);
  pstmt->executeUpdate();


  stmt = con->createStatement();
  res = stmt->executeQuery("SELECT MAX(id) FROM hive_satellite");
  while (res->next()) {
    cout << "\t... MySQL replies: ";
    cout << res->getInt(1) << endl;
    if(res->getInt(1)==newIDtoPut){
      cout << "New Hive Put SQL Success: " << newIDtoPut << endl;
    }else{
    cout << "Weird ID: " << newIDtoPut << " SQL ID: " << res->getInt(1) << endl;
    }
  }

  delete res;
  delete stmt;
  delete pstmt;
  delete con;
  
} 
catch (sql::SQLException &e) {
  cout << "# ERR: SQLException in " << __FILE__;
 // cout << "(" << __FUNCTION__ << ") on line " »
   //  << __LINE__ << endl;
  cout << "# ERR: " << e.what();
  cout << " (MySQL error code: " << e.getErrorCode();
  cout << ", SQLState: " << e.getSQLState() << " )" << endl;
}
}

int getLastID(){
  mesh.update();
  mesh.DHCP();
  try {
  sql::Driver *driver;
  sql::Connection *con;
  sql::Statement *stmt;
  sql::ResultSet *res;

  // Create a connection 
  driver = get_driver_instance();
  cout << "Get Last Hive From SQL" << endl;
  con = driver->connect("tcp://155.94.213.167:3306", "asianhornet", "aRpa18");
  // Connect to the MySQL test database 
  con->setSchema("ruchedb");
  stmt = con->createStatement();
  res = stmt->executeQuery("SELECT MAX(id) FROM hive_satellite");
  while (res->next()) {
    cout << "\t... MySQL replies: ";
    // Access column data by alias or column name 
    cout << res->getInt(1) << endl;
    
    return res->getInt(1);
  }
  delete res;
  delete stmt;
  delete con;
} 
catch (sql::SQLException &e) {
  cout << "# ERR: SQLException in " << __FILE__;
 // cout << "(" << __FUNCTION__ << ") on line " »
   //  << __LINE__ << endl;
  cout << "# ERR: " << e.what();
  cout << " (MySQL error code: " << e.getErrorCode();
  cout << ", SQLState: " << e.getSQLState() << " )" << endl;
  return 254;
  }
}


void startAP(int fromNode){
  printf("Start AP\n");
  mesh.update();
  mesh.DHCP();
    
    newID = getLastID();
    newID++;
    RF24NetworkHeader header(fromNode); //Constructing a header
    if(!network.write(header, &newID, sizeof(newID))){
      printf("Fail to send AP ID: %d To: %d\n",newID,fromNode);
    }else{
      printf("Send AP OK ID: %d To: %d\n",newID,fromNode);
      if(newID!=254){
      putNewHive(newID);
      } else {
        printf("Fail to fetch SQL");
      }
    }
}