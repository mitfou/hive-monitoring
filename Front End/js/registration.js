$(function(){
    var submit = $("#formRegister").on('submit', function (e){
        var form = $(this).serializeArray();
        if (form[1].value !== form[2].value){
            $("#idInvalid").show();
        }
        else {
            $.ajax({
                type: "POST",
                url:"http://localhost:3000/api/user/create",
                crossDomain: true,
                dataType: "JSON",
                data: {
                    "email": form[0].value,
                    "password": form[1].value
                },
                success: function (data){
                    $("#idValid").show();
                    $("#idInvalid").hide();
                },
                error: function (err){
                    $("#idValid").hide();
                    $("#idInvalid").show();
                }
            });
            e.preventDefault();
        }
    });

    var connect = $("#idConnect").on('click', function (e){
        window.location.replace("http://localhost:8080/connexion.html");
    });
});