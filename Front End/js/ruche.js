$(function () {
    var token = localStorage.getItem('token');
    var profile = undefined;
    var display = ["#displayHives", "#displayProfile", "#displayAlerts", "#displaySatellite"];
    var currenthive = 0;
    if (token != undefined) {
        $.ajax({
            type: "GET",
            url: "http://localhost:3000/api/user/isAuthenticate",
            crossDomain: true,
            headers: {
                'Authorization': token
            },
            success: function (data) {
                initMainPage();
            },
            error: function (err) {
                window.location.replace("http://localhost:8080/connexion.html");
            }
        });
    }
    else {
        window.location.replace("http://localhost:8080/connexion.html");
    }

    var toggleDisplay = function (disp) {
        display.forEach(function (id) {
            if (disp == id) {
                $(id).show();
            }
            else {
                $(id).hide();
            }
        });
    }

    var initMainPage = function () {
        $.ajax({
            type: "GET",
            url: "http://localhost:3000/api/profile",
            crossDomain: true,
            headers: {
                'Authorization': token
            },
            success: function (data) {
                profile = data;
                $("#idFullname").text(profile.firstname + " " + profile.lastname);
            },
            error: function (err) {
                console.err(err);
            }
        });
    }


    var logout = $("#idLogOut").on('click', function (e) {
        $.ajax({
            type: "GET",
            url: "http://localhost:3000/api/user/logout",
            crossDomain: true,
            headers: {
                'Authorization': token
            },
            success: function (data) {
            },
            error: function (err) {
                console.error("Error: Internal Server Error");
            }
        });
    });

    var deleteSatellite = function (element) {
        var id = element.find("span").attr("data-id");
        $.ajax({
            type: "DELETE",
            url: "http://localhost:3000/api/satellite/" + id,
            crossDomain: true,
            headers: {
                'Authorization': token
            },
            success: function (data) {
                element.remove();
            },
            error: function (err) {
                console.error("Error: Internal Server Error");
            }
        });
    }

    var displaySatellite = function (sats, hive) {
        toggleDisplay("#displaySatellite");
        var div = $("#displaySatellite");
        div.empty();
        var linkadd = $(`<a href="#" data-id=${hive.id} class="w3-bar-item w3-button w3-padding"><i class="fas fa-plus-square fa-fw"></i>  Ajouté une ruche</a>`);
        linkadd.on('click', function (e) {
            currenthive = $(this).attr('data-id');
            $("#modAddSatellite").show();
        });
        div.append(linkadd);
        sats.forEach(element => {
            var sat = $(`<div class="w3-quarter w3-margin-bottom">
                    <div class="w3-card w3-dark-grey">
                        <div class="w3-container w3-orange">
                            <span data-id="${element.id}" style="margin-top: 16px;" class="w3-button w3-right">&times;</span>
                            <h1>Ruche fille ${element.name}</h1>
                        </div>
                    <div class="w3-container">
                        <p>Poids ${element.weight}</p>
                    </div>
                    <div class="w3-container w3-blue-grey">
                        <p>Température ${element.temperature}</p>
                    </div>
                </div>
            </div>`);
            sat.find("span").on('click', function (e) {
                deleteSatellite(sat);
            });
            div.append(sat);
        });
    }

    var getSatelliteOfHive = function (hive) {
        $.ajax({
            type: "GET",
            url: "http://localhost:3000/api/satellite/" + hive.id,
            crossDomain: true,
            headers: {
                'Authorization': token
            },
            success: function (data) {
                displaySatellite(data, hive);
            },
            error: function (err) {
                console.error("Error: Internal Server Error");
            }
        });
    }

    var getHivesOfUser = function () {
        $.ajax({
            type: "GET",
            url: "http://localhost:3000/api/hive",
            crossDomain: true,
            headers: {
                'Authorization': token
            },
            success: function (data) {
                displayHives(data);
            },
            error: function (err) {
                console.error("Error: Internal Server Error");
            }
        });
    }

    var deleteHive = function (element) {
        var id = element.find("span").attr("data-id");
        $.ajax({
            type: "DELETE",
            url: "http://localhost:3000/api/hive/" + id,
            crossDomain: true,
            headers: {
                'Authorization': token
            },
            success: function (data) {
                element.remove();
            },
            error: function (err) {
                console.error("Error: Internal Server Error");
            }
        });
    }

    var displayHives = function (hives) {
        var div = $("#displayHives");
        div.empty();
        hives.forEach(element => {
            var hive = $(`<div class="w3-quarter w3-margin-bottom">
                    <div class="w3-card w3-dark-grey">
                        <div class="w3-container w3-orange">
                            <span data-id="${element.id}" style="margin-top: 16px;" class="w3-button w3-right">&times;</span>
                            <h1>Rucher ${element.name}</h1>
                        </div>
                    <div class="w3-container">
                        <p>Température ${element.temperature}</p>
                    </div>
                    <div class="w3-container w3-blue-grey">
                        <p>Géolocalisation ${element.geolocalisation}</p>
                    </div>
                </div>
            </div>`);
            hive.find("span").on('click', function (e) {
                deleteHive(hive);
            });
            hive.on('click', function (e) {
                getSatelliteOfHive(element);
            });
            div.append(hive);
        });
    }

    var deleteAlert = function (element) {
        var id = element.find("span").attr("data-id");
        $.ajax({
            type: "DELETE",
            url: "http://localhost:3000/api/alert/" + id,
            crossDomain: true,
            headers: {
                'Authorization': token
            },
            success: function (data) {
                element.remove();
            },
            error: function (err) {
                console.error("Error: Internal Server Error");
            }
        });
    }


    var displayAlert = function (alerts) {
        var div = $('#displayAlerts');
        div.empty();
        alerts.forEach(function (element) {
            var isActive = "non active";
            if (element.active == 1) {
                isActive = "active";
            }
            var alert = $(`<div class="w3-row">
                    <div data-id="${element.id}" class="w3-half w3-card-4 w3-margin-bottom">
                        <header class="w3-container w3-orange">
                            <span data-id="${element.id}" style="margin-top: 16px;" class="w3-button w3-right">&times;</span>
                            <h1>
                                Alerte ${element.type}
                            </h1>
                        </header>
                    <div class="w3-container w3-blue-grey">
                        <p>alerte du ${new Date(element.timestamp).toLocaleString()} : ${isActive}</p>
                    </div>
                </div>
            </div>`);
            alert.find("span").on('click', function (e) {
                deleteAlert(alert);
            });
            div.append(alert);
        });
    }

    var getAlertsOfUser = function () {
        $.ajax({
            type: "GET",
            url: "http://localhost:3000/api/alert",
            crossDomain: true,
            headers: {
                'Authorization': token
            },
            success: function (data) {
                displayAlert(data);
            },
            error: function (err) {
                console.error("Error: Internal Server Error");
            }
        });
    }

    $("#btnHives").on('click', function (e) {
        getHivesOfUser();
        toggleDisplay("#displayHives");
    });

    $("#btnProfile").on('click', function (e) {
        toggleDisplay("#displayProfile");
        $("#inputFirstname").attr('value', profile.firstname);
        $("#inputLastname").attr('value', profile.lastname);
        $("#inputPhone").attr('value', profile.phonenumber);
    });

    $("#btnAlerts").on('click', function (e) {
        getAlertsOfUser();
        toggleDisplay("#displayAlerts");
    });

    $("#btnAddHive").on('click', function (e) {
        $("#modAddHive").show();
    })

    $("#btnAddSatellite").on('click', function (e) {
        $("#modAddSatellite").show();
    })

    $("#formAddHive").on('submit', function (e) {
        var form = $(this).serializeArray();
        $.ajax({
            type: "PUT",
            url: "http://localhost:3000/api/hive",
            crossDomain: true,
            dataType: "JSON",
            headers: {
                'Authorization': token
            },
            data: {
                "name": form[0].value
            },
            success: function (data) {
                $("#modAddHive").hide();
                getHivesOfUser();
            },
            error: function (err) {
                $("#alertAddHive").show();
                console.error(err);
            }
        });
        e.preventDefault();
    });

    $("#formAddSatellite").on('submit', function (e) {
        var form = $(this).serializeArray();
        if (currenthive != 0) {
            $.ajax({
                type: "PUT",
                url: "http://localhost:3000/api/satellite/" + currenthive,
                crossDomain: true,
                dataType: "JSON",
                headers: {
                    'Authorization': token
                },
                data: {
                    "name": form[0].value
                },
                success: function (data) {
                    $("#modAddSatellite").hide();
                    var hive = {id: currenthive};
                    getSatelliteOfHive(hive);
                },
                error: function (err) {
                    $("#alertAddSatellite").show();
                    console.error(err);
                }
            });
            e.preventDefault();
        }
    });

    $("#formProfile").on('submit', function (e) {
        var form = $(this).serializeArray();
        $.ajax({
            type: "PATCH",
            url: "http://localhost:3000/api/profile",
            crossDomain: true,
            dataType: "JSON",
            headers: {
                'Authorization': token
            },
            data: {
                "firstname": form[0].value,
                "lastname": form[1].value,
                "phone": form[2].value
            },
            success: function (data) {
                profile.firstname = form[0].value;
                profile.lastname = form[1].value;
                profile.phonenumber = form[2].value;
                $("#alertProfile").show();
            },
            error: function (err) {
                console.error(err);
            }
        });
        e.preventDefault();
    });

    getHivesOfUser();

});