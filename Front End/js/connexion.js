$(function(){
    var submit = $("#formLogin").on('submit', function (e){
        var form = $(this).serializeArray();
        $.ajax({
            type: "POST",
            url:"http://localhost:3000/api/user/login",
            crossDomain: true,
            dataType: "JSON",
            data: {
                "email": form[0].value,
                "password": form[1].value
            },
            success: function (data){
                localStorage.setItem('token', data.key);
                $("#idValid").show();
                $("#idInvalid").hide();
                window.location.replace("http://localhost:8080/ruche.html");
            },
            error: function (err){
                $("#idValid").hide();
                $("#idInvalid").show();
            }
        });
        e.preventDefault();
    });

    var register = $("#idRegister").on('click', function (e){
        window.location.replace("http://localhost:8080/registration.html");
    });
});