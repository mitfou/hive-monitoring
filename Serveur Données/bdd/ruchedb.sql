-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 26 juin 2018 à 15:33
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ruchedb`
--
CREATE DATABASE IF NOT EXISTS `ruchedb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ruchedb`;

DELIMITER $$
--
-- Procédures
--
DROP PROCEDURE IF EXISTS `all_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `all_user` ()  NO SQL
SELECT * FROM user$$

DROP PROCEDURE IF EXISTS `createOrUpdateProfile`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `createOrUpdateProfile` (IN `id` INT, IN `f_name` VARCHAR(150), IN `l_name` VARCHAR(150), IN `phone` VARCHAR(15))  NO SQL
BEGIN
REPLACE INTO `profile` (id, firstname, lastname, phonenumber)
VALUES (id, f_name, l_name, phone);
SELECT * FROM profile WHERE profile.id = id LIMIT 1;
END$$

DROP PROCEDURE IF EXISTS `createOrUpdateToken`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `createOrUpdateToken` (IN `id` INT(11))  BEGIN
	REPLACE INTO token (token.id, token.key, token.expire)
	VALUES (id, UUID(), TIMESTAMPADD(HOUR, 6, NOW()));
    SELECT * FROM token WHERE token.id = id;
END$$

DROP PROCEDURE IF EXISTS `createUser`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `createUser` (IN `email` VARCHAR(255), IN `pass` VARCHAR(60))  NO SQL
INSERT INTO user (user.email, user.password) VALUES (email, pass)$$

DROP PROCEDURE IF EXISTS `deleteAlertById`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteAlertById` (IN `u_id` INT, IN `a_id` INT)  NO SQL
DELETE al
FROM alert al
WHERE al.user_id = u_id
AND al.id = a_id$$

DROP PROCEDURE IF EXISTS `deleteHiveById`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteHiveById` (IN `u_id` INT, IN `h_id` INT)  NO SQL
DELETE FROM hive_base
WHERE user_id = u_id
AND id = h_id$$

DROP PROCEDURE IF EXISTS `deleteSatelliteById`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteSatelliteById` (IN `u_id` INT, IN `s_id` INT)  NO SQL
DELETE hs
FROM hive_satellite hs
INNER JOIN hive_base hb ON hb.id = hs.hive_base_id
WHERE hb.user_id = u_id
AND hs.id = s_id$$

DROP PROCEDURE IF EXISTS `deleteTokenByKey`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteTokenByKey` (IN `tok` VARCHAR(60))  NO SQL
UPDATE token SET token.expire = NOW() WHERE token.key = tok$$

DROP PROCEDURE IF EXISTS `getAlertsOfUser`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAlertsOfUser` (IN `u_id` INT)  NO SQL
SELECT *
FROM alert al
WHERE al.user_id = u_id$$

DROP PROCEDURE IF EXISTS `getHistoOfHive`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getHistoOfHive` (IN `u_id` INT, IN `h_id` INT, IN `d_start` TIMESTAMP, IN `d_end` TIMESTAMP)  NO SQL
SELECT bi.*
FROM base_info bi
INNER JOIN hive_base hb ON hb.id = bi.base_id
WHERE hb.user_id = u_id
AND bi.base_id = h_id
AND bi.timestamp BETWEEN d_start AND d_end
ORDER BY bi.timestamp ASC$$

DROP PROCEDURE IF EXISTS `getHistoOfSatelite`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getHistoOfSatelite` (IN `u_id` INT, IN `s_id` INT, IN `d_start` INT, IN `d_end` INT)  NO SQL
SELECT si.*
FROM satellite_info si
INNER JOIN hive_satellite hs ON hs.id = si.satellite_id
INNER JOIN hive_base hb ON hb.id = hs.hive_base_id 
WHERE hb.user_id = u_id
AND si.satellite_id = s_id
AND si.timestamp BETWEEN d_start AND d_end
ORDER BY si.timestamp ASC$$

DROP PROCEDURE IF EXISTS `getHiveById`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getHiveById` (IN `u_id` INT, IN `h_id` INT)  NO SQL
SELECT hb.id, hb.name, hb.temperature, hb.geolocalisation, hb.timestamp
FROM hive_base hb
WHERE hb.user_id = u_id
AND hb.id = h_id$$

DROP PROCEDURE IF EXISTS `getHivesOfUser`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getHivesOfUser` (IN `u_id` INT)  NO SQL
SELECT hb.id, hb.name, hb.temperature, hb.geolocalisation, hb.timestamp
FROM hive_base hb
WHERE hb.user_id = u_id$$

DROP PROCEDURE IF EXISTS `getProfileById`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getProfileById` (IN `u_id` INT)  NO SQL
SELECT * FROM profile pr WHERE pr.id = u_id$$

DROP PROCEDURE IF EXISTS `getSatelliteOfHive`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getSatelliteOfHive` (IN `u_id` INT, IN `h_id` INT)  NO SQL
SELECT hs.*
FROM hive_satellite hs
INNER JOIN hive_base hb on hs.hive_base_id = hb.id
WHERE hb.user_id = u_id
AND hs.hive_base_id = h_id$$

DROP PROCEDURE IF EXISTS `getTokenById`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getTokenById` (IN `id` INT)  NO SQL
SELECT * FROM token WHERE token.id = id$$

DROP PROCEDURE IF EXISTS `getUserByEmail`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getUserByEmail` (IN `email` VARCHAR(125))  NO SQL
SELECT * FROM user WHERE user.email LIKE email$$

DROP PROCEDURE IF EXISTS `getUserById`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getUserById` (IN `p_id` INT)  NO SQL
SELECT * FROM user WHERE p_id = user.id$$

DROP PROCEDURE IF EXISTS `getUserByToken`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getUserByToken` (IN `tok` VARCHAR(60))  NO SQL
SELECT u.id, u.email FROM token t
INNER JOIN user u
ON t.id = u.id
WHERE t.expire > NOW() and t.key = tok$$

DROP PROCEDURE IF EXISTS `insertHiveForUser`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertHiveForUser` (IN `u_id` INT, IN `h_name` VARCHAR(100))  NO SQL
INSERT INTO hive_base (`name`, `timestamp`, `user_id`) VALUES (h_name, NOW(), u_id)$$

DROP PROCEDURE IF EXISTS `insertSatelliteToHive`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertSatelliteToHive` (IN `u_id` INT, IN `h_id` INT, IN `s_name` VARCHAR(100))  NO SQL
INSERT INTO hive_satellite(name, hive_base_id)
SELECT s_name, h_id
FROM hive_base hb
WHERE hb.id = h_id
AND hb.user_id = u_id$$

DROP PROCEDURE IF EXISTS `isTokenValid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `isTokenValid` (IN `tok` VARCHAR(60))  NO SQL
SELECT EXISTS(SELECT * FROM token
WHERE token.key = tok
AND token.expire > NOW()
LIMIT 1) as Valid$$

DROP PROCEDURE IF EXISTS `updateHiveById`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateHiveById` (IN `u_id` INT, IN `h_id` INT, IN `h_name` VARCHAR(100))  NO SQL
UPDATE hive_base SET name = h_name
WHERE user_id = u_id
AND id = h_id$$

DROP PROCEDURE IF EXISTS `updateSatelliteById`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateSatelliteById` (IN `u_id` INT, IN `s_id` INT, IN `s_name` VARCHAR(100))  NO SQL
UPDATE hive_satellite hs
INNER JOIN hive_base hb ON hb.id = hs.hive_base_id
SET hs.name = s_name
WHERE hb.user_id = u_id
AND hs.id = s_id$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `alert`
--

DROP TABLE IF EXISTS `alert`;
CREATE TABLE `alert` (
  `id` int(11) NOT NULL,
  `type` enum('Temperature','Weight','GPS','') NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hive_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `alert`
--

INSERT INTO `alert` (`id`, `type`, `timestamp`, `active`, `user_id`, `hive_id`) VALUES
(1, 'Weight', '2018-06-13 10:15:59', 1, 7, 1),
(2, 'Temperature', '2018-06-13 10:15:59', 1, 7, 2);

-- --------------------------------------------------------

--
-- Structure de la table `base_info`
--

DROP TABLE IF EXISTS `base_info`;
CREATE TABLE `base_info` (
  `id` int(11) NOT NULL,
  `temperature` float NOT NULL,
  `geolocalisation` varchar(150) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `base_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `base_info`
--

INSERT INTO `base_info` (`id`, `temperature`, `geolocalisation`, `timestamp`, `base_id`) VALUES
(1, 20.3, 'une geolocalisation', '2018-03-21 09:50:01', 1),
(2, 24, 'la meme geolocalisation', '2018-03-21 09:50:28', 1),
(3, 15, 'azertgdsqgvds', '2018-03-03 23:00:00', 2),
(5, 17, 'azertgdsqgvds', '2018-03-21 13:35:32', 2),
(6, 16, 'fdnskqvdskqlvdsql', '2018-03-12 23:00:00', 2),
(7, 11, 'salle de classe', '2018-03-23 12:41:26', 1);

--
-- Déclencheurs `base_info`
--
DROP TRIGGER IF EXISTS `trigNewInfo`;
DELIMITER $$
CREATE TRIGGER `trigNewInfo` AFTER INSERT ON `base_info` FOR EACH ROW UPDATE hive_base hb
SET hb.temperature = NEW.temperature,
hb.geolocalisation = NEW.geolocalisation
WHERE hb.id = NEW.base_id
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `hive_base`
--

DROP TABLE IF EXISTS `hive_base`;
CREATE TABLE `hive_base` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `geolocalisation` varchar(150) DEFAULT NULL,
  `temperature` float DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `hive_base`
--

INSERT INTO `hive_base` (`id`, `name`, `geolocalisation`, `temperature`, `timestamp`, `user_id`) VALUES
(1, 'vigneux', 'salle de classe', 11, '2018-03-16 23:59:45', 7),
(2, 'arpajon', NULL, NULL, '2018-03-16 23:59:45', 7),
(5, 'draveil', NULL, NULL, '2018-03-23 14:37:20', 7),
(6, 'marseille', 'pas ici', 130, '2018-04-20 13:49:38', 7);

-- --------------------------------------------------------

--
-- Structure de la table `hive_satellite`
--

DROP TABLE IF EXISTS `hive_satellite`;
CREATE TABLE `hive_satellite` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `temperature` float DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hive_base_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `hive_satellite`
--

INSERT INTO `hive_satellite` (`id`, `name`, `weight`, `temperature`, `timestamp`, `hive_base_id`) VALUES
(1, 'sat vigneux 1', 10, 20, '2018-06-06 10:25:43', 1),
(2, 'sat vigneux 2', 11, 21, '2018-06-06 10:25:43', 1),
(3, 'sat arpajon 1', 10, 20, '2018-06-06 10:26:43', 2);

-- --------------------------------------------------------

--
-- Structure de la table `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `phonenumber` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `profile`
--

INSERT INTO `profile` (`id`, `firstname`, `lastname`, `phonenumber`) VALUES
(7, 'Michel', 'Gabelus', '0102030405'),
(18, 'Michel', 'Gabel', '0169020202');

-- --------------------------------------------------------

--
-- Structure de la table `satellite_info`
--

DROP TABLE IF EXISTS `satellite_info`;
CREATE TABLE `satellite_info` (
  `id` int(11) NOT NULL,
  `temperature` float NOT NULL,
  `weight` float NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `satellite_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `satellite_info`
--
DROP TRIGGER IF EXISTS `trigNewInfoSatellite`;
DELIMITER $$
CREATE TRIGGER `trigNewInfoSatellite` AFTER INSERT ON `satellite_info` FOR EACH ROW UPDATE hive_satellite hs
SET hs.weight = NEW.weight,
hs.temperature = NEW.temperature
WHERE hs.id = NEW.satellite_id
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `token`
--

DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `id` int(11) NOT NULL,
  `key` varchar(36) NOT NULL,
  `expire` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `token`
--

INSERT INTO `token` (`id`, `key`, `expire`) VALUES
(7, '94c71d9c-6fa1-11e8-bfb0-6c626d371e85', '2018-06-14 10:00:27'),
(10, 'b200e357-26a8-11e8-8918-6c626d371e85', '2018-03-13 11:23:56'),
(18, 'ee5f8672-6fa0-11e8-bfb0-6c626d371e85', '2018-06-14 07:03:20');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(125) NOT NULL,
  `password` varchar(60) NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '1',
  `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `verified`, `registration_date`) VALUES
(7, 'michel@gmail.com', '$2a$10$Aobnto2QTOCeXDhjlq3oyeSGrAFzis/raVGeGgOMN7OLIJeH3QAeK', 1, '2018-03-09 08:29:18'),
(10, 'mitfou@gmail.com', '$2a$10$Aobnto2QTOCeXDhjlq3oyeSGrAFzis/raVGeGgOMN7OLIJeH3QAeK', 1, '2018-03-09 08:40:40'),
(18, 'test@test.com', '$2a$10$EBuaU7ak8fHOBzs2/hRWGOJ.2YnIqtJC9vlD.rbMR3nMeQTuKLRfa', 1, '2018-06-14 07:01:33');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `alert`
--
ALTER TABLE `alert`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `hive_id` (`hive_id`);

--
-- Index pour la table `base_info`
--
ALTER TABLE `base_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `base_id` (`base_id`);

--
-- Index pour la table `hive_base`
--
ALTER TABLE `hive_base`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `hive_satellite`
--
ALTER TABLE `hive_satellite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hive_base_id` (`hive_base_id`);

--
-- Index pour la table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `satellite_info`
--
ALTER TABLE `satellite_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `satellite_id` (`satellite_id`);

--
-- Index pour la table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `alert`
--
ALTER TABLE `alert`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `base_info`
--
ALTER TABLE `base_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `hive_base`
--
ALTER TABLE `hive_base`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `hive_satellite`
--
ALTER TABLE `hive_satellite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `satellite_info`
--
ALTER TABLE `satellite_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `alert`
--
ALTER TABLE `alert`
  ADD CONSTRAINT `alert_ibfk_1` FOREIGN KEY (`hive_id`) REFERENCES `hive_base` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `alert_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `base_info`
--
ALTER TABLE `base_info`
  ADD CONSTRAINT `base_info_ibfk_1` FOREIGN KEY (`base_id`) REFERENCES `hive_base` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `hive_base`
--
ALTER TABLE `hive_base`
  ADD CONSTRAINT `hive_base_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `hive_satellite`
--
ALTER TABLE `hive_satellite`
  ADD CONSTRAINT `hive_satellite_ibfk_1` FOREIGN KEY (`hive_base_id`) REFERENCES `hive_base` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `satellite_info`
--
ALTER TABLE `satellite_info`
  ADD CONSTRAINT `satellite_info_ibfk_1` FOREIGN KEY (`satellite_id`) REFERENCES `hive_satellite` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `token_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
