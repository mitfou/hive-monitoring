var express = require('express');
var passport = require('../passport/passport');
var profiledb = require('./profile.db');
var validate = require('../joi/validate');
var schema = require('./profile.sch');
var router = express.Router();

/* Middleware de recherche d'utilisateur 
** Il insere l'id et l'email de l'utilisateur dans chaque requète
** en les retrouvant grâce à la clé api
*/
var getUserId = require('../helper/getUserId');

router.use(passport.authenticate('token', {session: false}));
router.use(getUserId);

router.get('/', function (req, res){
    profiledb.getProfileById(req.user.id, (err, results, fields) => {
        if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
        else {
            res.status(200).json(results[0][0] || {});
        }
    });
});

router.patch('/', validate(schema.profile), function (req, res){
    profiledb.createOrUpdateProfile(req.user.id, req.body.firstname, req.body.lastname, req.body.phone,
        (err, results, fields) => {
            if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
            else {
                res.status(200).json(results[0][0] || {});
            }   
        });
});


module.exports = router;