var joi = require('joi');

var schema = {
    profile : {
        firstname: joi.string().min(3).max(100).required(),
        lastname: joi.string().min(3).max(100).required(),
        phone: joi.string().min(8).max(100).required()
    }
}

module.exports = schema;