var db = require('../db/db');

var profiledb = {
    getProfileById: function (u_id, cb){
        db.query('CALL getProfileById (?)', [u_id], cb);
    },

    createOrUpdateProfile: function (u_id, f_name, l_name, phone, cb){
        db.query('CALL createOrUpdateProfile (?, ?, ?, ?)', [u_id, f_name, l_name, phone], cb);
    }
};

module.exports = profiledb;