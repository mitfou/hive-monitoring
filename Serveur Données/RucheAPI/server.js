var bodyParser = require('body-parser');
var express = require('express');
var morgan = require('morgan');
var app = express();

//morgan loggin middleware 
app.use(morgan('dev', {
    skip: function (req, res) {
        return res.statusCode < 400
    }, stream: process.stderr
}));

app.use(morgan('dev', {
    skip: function (req, res) {
        return res.statusCode >= 400
    }, stream: process.stdout
}));

//swaggerUI
var swaggerUi = require('swagger-ui-express');
var swaggerDoc = require('./swagger/swagger.json');
app.use('/api/api-doc', swaggerUi.serve, swaggerUi.setup(swaggerDoc));

// Routes
var hive = require('./hive/hive');
var satellite = require('./satellite/satellite');
var user = require('./user/user');
var profile = require('./profile/profile');
var alert = require('./alert/alert');
var histo = require('./histo/histo');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(function(req, res, next) {
    //Ajoute les headers suivant pour permettre les requêtes CORS
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, x-requested-with, x-requested-by, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS");
    //accepte la methode OPTIONS
    if ('OPTIONS' === req.method) {
        res.status(200).send();
    }
    else {
        next();
    }
});

app.use('/api/user', user);
app.use('/api/hive', hive);
app.use('/api/profile', profile);
app.use('/api/alert', alert);
app.use('/api/satellite', satellite);
app.use('/api/histo', histo);

app.get('/', function (req, res){
    res.redirect('/api/api-doc');
});

// middleware de gestion d'erreurs
app.use(function (err, req, res, next){
    if (err.name == "ValidationError"){
        res.status(400).json({code: 400, msg: "Error: Bad Request"});
    }
    else next(err);
});

app.use(function (err, req, res, next){
    res.status(500).json({code: 500, msg: "Error: Internal Server Error"});
});

app.listen(3000, function (){
    console.log('SERVER: listening on port 3000');
});