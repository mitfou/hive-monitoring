var joi = require('joi');

var validate = function (schema, option){
    return function (req, res, next){
        option = option || {};
        const result = joi.validate(req.body, schema, option);
        if (result.error) next(result.error);
        else {
            next();
        }
    }
}

module.exports = validate;