var db = require('../db/db');

var alertdb = {
    getAlertsOfUser: function (u_id, cb){
         db.query('CALL getAlertsOfUser(?)', [u_id], cb);
    },
    deleteAlertById: function (u_id, a_id, cb){
        db.query('CALL deleteAlertById(?, ?)', [u_id, a_id], cb);
    }
};

module.exports = alertdb;