var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "admin",
  password: "azerty",
  database: "ruchedb"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("SERVER: Connexion to Db established");
});

module.exports = con;           