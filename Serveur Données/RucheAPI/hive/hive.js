var express = require('express');
var passport = require('../passport/passport');
var userdb = require('../user/user.db');
var hivedb = require('./hive.db');
var validate = require('../joi/validate');
var schema = require('./hive.sch');
var isNumber = require('../helper/isNumber');
var router = express.Router();

/* Middleware de recherche d'utilisateur 
** Il insere l'id et l'email de l'utilisateur dans chaque requète
** en les retrouvant grâce à la clé api
*/
var getUserId = require('../helper/getUserId');

router.use(passport.authenticate('token', {session: false}));
router.use(getUserId);

router.get('/', function (req, res){
    hivedb.getHivesOfUser(req.user.id, (err, results, fields) => {
        if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
        else {
            res.status(200).json(results[0]);
        }
    });
});

router.get('/:id', function (req, res){
    if (!isNumber(req.params.id)) res.status(400).json({code: 400, msg: "Error: Bad Request"});
    else {
        hivedb.getHiveById(req.user.id, req.params.id, (err, results, fields) =>{
            if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
            else {
                res.status(200).json(results[0][0]);
            }
        });
    }
});

router.put('/', validate(schema.hivePut), function (req, res){
    hivedb.insertHiveForUser(req.user.id, req.body.name, (err, results, fields) => {
        if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
        else {
            res.status(201).json({code: 201, msg:"Ressource created"});
        }
    });
});

router.delete('/:id', function (req, res){
    if (!isNumber(req.params.id)) res.status(400).json({code: 400, msg: "Error: Bad Request"});
    else {
        hivedb.deleteHiveById(req.user.id, req.params.id, (err, results, fields) =>{
            if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
            else {
                res.status(200).json({code: 200, msg:"Ressource deleted"});
            }
        });
    }
});

router.patch('/:id', validate(schema.hivePut), function (req, res){
    if (!isNumber(req.params.id)) res.status(400).json({code: 400, msg: "Error: Bad Request"});
    else {
        hivedb.updateHiveById(req.user.id, req.params.id, req.body.name, (err, results, fields) =>{
            if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
            else {
                res.status(200).json({code: 200, msg:"Ressource modified"});
            }
        });
    }
});

module.exports = router;