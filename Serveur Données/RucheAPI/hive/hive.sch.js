var joi = require('joi');

var schema = {
    hivePut : {
        name: joi.string().min(3).max(100).required()
    }
}

module.exports = schema;