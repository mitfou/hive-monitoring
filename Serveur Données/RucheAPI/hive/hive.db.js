var db = require('../db/db');

var hivedb = {
    getHivesOfUser: function (u_id, cb){
        db.query('CALL getHivesOfUser (?)', [u_id], cb);
    },

    insertHiveForUser: function (u_id, name, cb){
        db.query('CALL insertHiveForUser (?, ?)', [u_id, name], cb);
    },

    getHiveById: function (u_id, h_id, cb){
        db.query('CALL getHiveById (?, ?)', [u_id, h_id], cb);
    },

    deleteHiveById: function (u_id, h_id, cb){
        db.query('CALL deleteHiveById (?, ?)', [u_id, h_id], cb);
    },

    updateHiveById: function (u_id, h_id, name, cb){
        db.query('CALL updateHiveById (?, ?, ?)', [u_id, h_id, name], cb);
    }
};

module.exports = hivedb;