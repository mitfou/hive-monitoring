var db = require('../db/db');

var userdb = {
    all_user: function (cb){
        db.query("CALL all_user", cb);
    },

    createUser: function (email, password, cb){
        db.query('CALL createUser (?, ?)', [email, password], cb);
    },

    getUserByEmail: function (email, cb){
        db.query('CALL getUserByEmail (?)', [email], cb);
    },

    getUserById: function (id, cb){
        db.query('CALL getUserByEmail (?)', [id], cb);
    },

    createOrUpdateToken: function (id, cb){
        db.query('CALL createOrUpdateToken (?)', [id], cb);
    },

    isTokenValid: function (tok, cb){
        db.query('CALL isTokenValid (?)', [tok], cb);
    },

    deleteTokenByKey: function (tok, cb){
        db.query('CALL deleteTokenByKey (?)', [tok], cb);
    },

    getUserIdByToken: function (tok, cb){
        db.query('CALL getUserByToken (?)', [tok], cb);
    }
};

module.exports = userdb;