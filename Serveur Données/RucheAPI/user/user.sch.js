var joi = require('joi');

var schema = {
    userCreate : {
        email: joi.string().email().min(6).max(125).required(),
        password: joi.string().min(6).max(32),
    }
}

module.exports = schema;