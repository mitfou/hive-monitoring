var express  = require('express');
var bcrypt   = require('bcryptjs');
var validate = require('../joi/validate');
var userdb   = require('./user.db');
var schema   = require('./user.sch');
var passport = require('../passport/passport');

var router = express.Router();

var create = function (req, res){
    bcrypt.hash(req.body.password, 10, (err, hash) => {
        if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
        else {
            userdb.createUser(req.body.email, hash, (err, results, fields) =>{
                if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
                else {
                    res.status(201).json({code: 201, msg:"Ressource created"});
                }
            });
        }
    });
}

var login = function (req, res){
    userdb.getUserByEmail(req.body.email, (err, results, fields) => {
        if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
        else {
            user = results[0][0];
            if (user){
                bcrypt.compare(req.body.password, user.password, (err, pass) => {
                    if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
                    else {
                        if (pass == false) res.status(400).json({code: 400, msg: "Error: username or password incorrect"});
                        else {
                            userdb.createOrUpdateToken(user.id, (err, token, fields) =>{
                                if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
                                else {
                                    res.status(200).json(token[0][0]);
                                }
                            })
                        }
                    }
                });
            }
            else {
                res.status(400).json({code: 400, msg: "Error: username or password incorrect"});
            }
        }
    });
}

var logout = function (req, res){
    userdb.deleteTokenByKey(req.headers['authorization'], (err, results, fields) => {
        if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
        else {
            res.status(200).json({code: 200, msg:"Ok"});
        }
    });
}

var isAuthenticate = function (req, res){
    res.status(200).json({code: 200, msg: "Ok"});
}

router.post('/create', validate(schema.userCreate), create);
router.post('/login', validate(schema.userCreate), login);
router.get('/logout', passport.authenticate('token', {session: false}), logout);
router.get('/isAuthenticate', passport.authenticate('token', {session: false}), isAuthenticate);

module.exports = router;