var passport = require('passport');
var customStrategy = require('passport-custom').Strategy; 
var userdb = require('../user/user.db'); 
 
var token = new customStrategy(function (req, next){ 
    var apiToken = req.headers["authorization"];
    if (apiToken){ 
        userdb.isTokenValid(apiToken, (err, results, fields) => { 
            if (err) next(err); 
            else if (results[0][0].Valid == 0){ 
                next(null, false, {code: 400, message:"Error: Bad Request"}); 
            } 
            else { 
                next(null, true); 
            } 
        }); 
    } 
    else { 
        next(null, false, {code: 400, message:"Error: Bad Request"}); 
    } 
}); 
 
passport.use('token', token); 
 
module.exports = passport;