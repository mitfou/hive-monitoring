var express = require('express');
var passport = require('../passport/passport');
var userdb = require('../user/user.db');
var histodb = require('./histo.db');
var validate = require('../joi/validate');
var schema = require('./histo.sch');
var isNumber = require('../helper/isNumber');
var router = express.Router();

/* Middleware de recherche d'utilisateur 
** Il insere l'id et l'email de l'utilisateur dans chaque requète
** en les retrouvant grâce à la clé api
*/
var getUserId = require('../helper/getUserId');

router.use(passport.authenticate('token', {session: false}));
router.use(getUserId);

router.get('/hive/:id', function (req, res){
    if (!isNumber(req.params.id)
        || req.query.start == undefined
        || req.query.end == undefined) {
            res.status(400).json({code: 400, msg: "Error: Bad Request"});
        }
    histodb.getHistoOfHive(req.user.id, req.params.id, req.query.start, req.query.end, (err, results, fields) => {
        if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
        else {
            res.status(200).json(results[0]);
        }
    });
});

router.get('/satellite/:id', function (req, res){
    if (!isNumber(req.params.id)
        || req.query.start == undefined
        || req.query.end == undefined) {
            res.status(400).json({code: 400, msg: "Error: Bad Request"});
        }
        histodb.getHistoOfSatellite(req.user.id, req.params.id, req.query.start, req.query.end, (err, results, fields) => {
        if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
        else {
            res.status(200).json(results[0]);
        }
    });
});

module.exports = router;