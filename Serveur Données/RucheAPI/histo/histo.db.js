var db = require('../db/db');

var histodb = {
    getHistoOfHive: function (u_id, h_id, d_start, d_end, cb){
        db.query('CALL getHistoOfHive (?, ?, ?, ?)', [u_id, h_id, d_start, d_end], cb);
    },

    getHistoOfSatellite: function (u_id, s_id, d_start, d_end, cb){
        db.query('CALL getHistoOfSatellite (?, ?, ?, ?)', [u_id, s_id, d_start, d_end,], cb);
    },
};

module.exports = histodb;