var db = require('../db/db');

var satdb = {
    getSatelliteOfHive: function (u_id, h_id, cb){
         db.query('CALL getSatelliteOfHive(?, ?)', [u_id, h_id], cb);
    },
    deleteSatelliteById: function (u_id, s_id, cb){
        db.query('CALL deleteSatelliteById(?, ?)', [u_id, s_id], cb);
    },
    updateSatelliteById: function (u_id, s_id, name, cb){
        db.query('CALL updateSatelliteById(?, ?, ?)', [u_id, s_id, name], cb);
    },
    insertSatelliteToHive: function (u_id, s_id, name, cb){
        db.query('CALL insertSatelliteToHive(?, ?, ?)', [u_id, s_id, name], cb);
    }
};

module.exports = satdb;