var express = require('express');
var passport = require('../passport/passport');
var userdb = require('../user/user.db');
var satdb = require('./satellite.db');
var validate = require('../joi/validate');
var schema = require('./satellite.sch');
var isNumber = require('../helper/isNumber');
var router = express.Router();

/* Middleware de recherche d'utilisateur 
** Il insere l'id et l'email de l'utilisateur dans chaque requète
** en les retrouvant grâce à la clé api
*/
var getUserId = require('../helper/getUserId');

router.use(passport.authenticate('token', {session: false}));
router.use(getUserId);

router.get('/:hiveid', function (req, res){
    if (!isNumber(req.params.hiveid)) res.status(400).json({code: 400, msg: "Error: Bad Request"});
    satdb.getSatelliteOfHive(req.user.id, req.params.hiveid, (err, results, fields) => {
        if (err) res.status(500).json({code: 500, msg:"Error: Internal Server Error"});
        else {
            res.status(200).json(results[0]);
        }
    });
});

router.delete('/:id', function (req, res){
    if (!isNumber(req.params.id)) res.status(400).json({code: 400, msg: "Error: Bad Request"});
    satdb.deleteSatelliteById(req.user.id, req.params.id, (err, results, fields) => {
        if (err) res.status(500).json({code: 500, msg: "Error: Internal Server Error"});
        else {
            res.status(200).json({code: 200, msg:"Ressource deleted"});            
        }
    });
});

router.patch('/:id', validate(schema.satPatch), function (req, res){
    if (!isNumber(req.params.id)) res.status(400).json({code: 400, msg: "Error: Bad Request"});
    satdb.updateSatelliteById(req.user.id, req.params.id, req.body.name, (err, results, fields) => {
        if (err) res.status(500).json({code: 500, msg: "Error: Internal Server Error"});
        else {
            res.status(204).json({code: 204, msg:"Ressource modified"});
        }
    });
});

router.put('/:hiveid', validate(schema.satPut), function (req, res){
    if (!isNumber(req.params.hiveid)) res.status(400).json({code: 400, msg: "Error: Bad Request"});
    satdb.insertSatelliteToHive(req.user.id, req.params.hiveid, req.body.name, (err, results, fields) => {
        if (err) res.status(500).json({code: 500, msg: "Error: Internal Server Error"});
        else {
            res.status(201).json({code: 201, msg:"Ressource created"});
        }
    });    
});


module.exports = router;