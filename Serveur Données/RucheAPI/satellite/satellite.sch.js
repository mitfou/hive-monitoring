var joi = require('joi');

var schema = {
    satPut : {
        name: joi.string().min(3).max(100).required(),
    },
    satPatch : {
        name: joi.string().min(3).max(100).required()
    }
};

module.exports = schema;