var userdb = require('../user/user.db');

var getUserId = function (req, res, next){
    userdb.getUserIdByToken(req.headers['authorization'], (err, results, fields) => {
        if (err) next(err);
        else {
            var user = {id : results[0][0].id, email: results[0][0].email}
            if (user.id){
                req.user = user;
                next();
            }
            else {
                next({code: 500, msg: "Error: Internal Server Error"})
            }
        }
    });
}

module.exports = getUserId;