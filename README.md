# Projet Ruche

Surveillance des ruches à distance

## Expression du besoin

Un rucher est composé de 4 ou 5 ruches (maximum 10), La distance entre 2 ruches varie de 2 à 3 mètres. Chaque ruche peut contenir 10000 à 60000 abeilles. Les ruches sont en plastiques et leur masse varient de 30Kg (Vide) à 150Kg (Pleine). 
L'apiculteur souhaite un système lui permettant de surveiller à distance ses ruchers. Il a besoin des informations suivantes:
	
	- Température et Humidité extérieure du rucher
	- Géolocalisation du rucher (Uniquement la ruche mère)
	- Masse de chaque ruche
	- Température intérieure de chaque ruches. Normale: 35°C Alarme: 39°C à 42°C lors de l'essaimage

### Contraintes

Le système est alimenté par des batteries de 12V (LifePO4) rechargées par des panneaux solaires.

Les ruches doivent communiquer entre elles via un pico-réseau.

La ruche mère transmet ces informations à un serveur de données distant que l'apiculteur peut consulter via une interface WEB.

L'apiculteur doit être informé par SMS En cas de chute de la masse de la ruche (vol) et de risque d'essaimage (masse trop importante et/ou température trop haute).


## Objectif

Réaliser une maquette permettant de valider la mise en place de l'architecture finale.

### Contraintes de réalisation

	 Ruches "filles":
		- Arduino + capteur de température + capteur de masse + NRF24L01+ (2400MHz)
	
	 Ruche "Mère":
		- Rasperry Pi + capteur température et humidité + GPS + GSM + NRF24L01+ (2400MHz)
	
	 Base de données: 
		- MySQL

## Technologies

* [NodeJS](https://nodejs.org) - Open-source server environment
* [MariaDB](https://mariadb.org) - Open-source database management system SQL compliant

* [Arduino](https://arduino.cc) - Open-source electronics platform
* [Raspberry Pi](https://raspberrypi.org) - Small and affordable ARMv7 computer


## Autheurs

* **Jean LE QUELLEC** - *Centralisation des mesures* - [Naejel](https://gitlab.com/naejel)
* **Michel GABELUS** - *Serveur de données* - [Mitfou](https://gitlab.com/mitfou)
* **Alexandre LEGENDRE** - *Informations du rucher*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details